<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GanjilGenapController extends Controller
{
    public function index(){
        return view('formGenapGanjil');
    }

    public function cekGenapGanjil(Request $request){

        $start = $request->start;
        $end = $request->end;

        $hasil = [];

        if($start < $end){
            for($i = $start; $i <= $end; $i++){

                if($i % 2 == 0){
                    $hasil [] = "Angka ".$i." adalah genap";
                }else{
                    $hasil [] = "Angka ".$i." adalah ganjil";
                }
            }
        }

        return view('hasil', compact('hasil'));
    }
}
