<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Genap Ganjil</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="{{ asset('style/styleku.css') }}">
</head>

<body>
    <div id="card">
        <div id="card-content">
            <div id="card-title">
                <h2>Genap Ganjil</h2>
                <div class="underline-title"></div>
            </div>
      
            <form action="{{ route('cek') }}" method="post" class="form">
            @csrf
                <label style="padding-top:13px">
                    &nbsp;Start
                </label>
                <input id="start" class="form-content" type="number" name="start" autocomplete="on" required />
                <div class="form-border"></div>
                <label style="padding-top:22px">&nbsp;End
                </label>
                <input id="end" class="form-content" type="number" name="end" autocomplete="on" required />
                <div class="form-border"></div>

                <input id="submit-btn" type="submit" name="submit" value="Cek"/>
            </form>
        </div>
    </div>
</body>

</html>