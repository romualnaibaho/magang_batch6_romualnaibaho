<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Genap Ganjil</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="{{ asset('style/styleku.css') }}">
</head>

<style>
    table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
</style>

<body>
    <div id="card">
        <div id="card-content">
            <div id="card-title">
                <h2>Hasil</h2>
                <div class="underline-title"></div>
            </div>
                
            <div style="text-align: center">
                <table>
                    <thead>
                        <tr>
                            <th id="1" style="text-align:center;">Hasil</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($hasil as $hasil)
                        <tr>
                            <td>{{ $hasil }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td>Start Number must be less than end number !</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>

                <a href="{{ route('index') }}" style="display: flex;">
                    <input id="submit-btn" type="submit" name="submit" value="Kembali"/>
                </a>

        </div>
    </div>
</body>

</html>