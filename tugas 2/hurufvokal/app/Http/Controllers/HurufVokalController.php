<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HurufVokalController extends Controller
{
    public function index(){

        return view('hurufVokal');
    }

    public function cekHurufVokal(Request $request){

        $kata = htmlspecialchars($request->kata);

        $arr = str_split($kata);
        $vocal = ['a', 'i', 'u', 'e', 'o'];

        $found = array_intersect($vocal, $arr);

        $jumlah = count($found);

        return view('hasil', compact('kata', 'found', 'jumlah'));
    }
}
