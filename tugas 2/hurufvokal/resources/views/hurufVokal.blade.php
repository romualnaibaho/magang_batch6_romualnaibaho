<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Huruf Vokal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="{{ asset('style/styleku.css') }}">
</head>

<body>
    <div id="card">
        <div id="card-content">
            <div id="card-title">
                <h2>Huruf Vokal</h2>
                <div class="underline-title"></div>
            </div>
      
            <form action="{{ route('cek') }}" method="post" class="form">
            @csrf
                <label style="padding-top:13px">
                    &nbsp;Masukkan Kata
                </label>
                <input id="kata" class="form-content" type="text" name="kata" autocomplete="on" required />
                
                <div class="form-border"></div>

                <input id="submit-btn" type="submit" name="submit" value="Cek Huruf Vokal"/>
            </form>
        </div>
    </div>
</body>

</html>