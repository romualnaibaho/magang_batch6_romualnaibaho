<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalkulatorController extends Controller
{
    public function index(){

        return view('kalkulator');
    }

    public function cekHasil(Request $request){

        $temp = explode(" ", $request->input);
        $bil1 = $temp[0];
        $operator = $temp[1];
        $bil2 = $temp[2];

        $hasil=0;

        if ($operator == "+"){
            $hasil = $bil1 + $bil2;
        }else if ($operator == "-"){
            $hasil = $bil1 - $bil2;
        }else if ($operator == "x" || $operator == "X" || $operator == "*"){
            $hasil = $bil1 * $bil2;
        }else if ($operator == "/" || $operator == ":"){
            if($bil2 != 0){
                $hasil = $bil1 / $bil2;
            }else{
                $hasil = "tidak bisa dilakukan";
            }
        }

        return view('hasil', compact('hasil', 'bil1', 'bil2', 'operator'));
    }
}
