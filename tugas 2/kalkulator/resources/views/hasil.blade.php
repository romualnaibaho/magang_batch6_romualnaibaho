<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Kalkulator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="{{ asset('style/styleku.css') }}">
</head>

<body>
    <div id="card">
        <div id="card-content">
            <div id="card-title">
                <h2>Hasil</h2>
                <div class="underline-title"></div>
            </div>
                
            <div class="hasil">
                <p>{{$bil1}} {{$operator}} {{$bil2}}</p>
                <p>=</p>
                <p>{{$hasil}}</p>
            </div>

            <a href="{{ route('index') }}" style="display: flex;">
                <input id="submit-btn" type="submit" name="submit" value="Kembali"/>
            </a>
        </div>
    </div>
</body>

</html>