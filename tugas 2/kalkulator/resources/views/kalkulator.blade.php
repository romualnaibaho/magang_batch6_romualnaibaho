<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Kalkulator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="{{ asset('style/styleku.css') }}">
</head>

<body>
    <div id="card">
        <div id="card-content">
            <div id="card-title">
                <h2>Kalkulator</h2>
                <div class="underline-title"></div>
            </div>
      
            <form action="{{ route('cek') }}" method="post" class="form">
            @csrf
                <label style="padding-top:13px">
                    &nbsp;Masukkan Operasi Yang Diinginkan
                </label>
                <input id="input" class="form-content" type="text" name="input" autocomplete="on" placeholder="2 x 12" required />
                <div class="form-border"></div>

                <input id="submit-btn" type="submit" name="submit" value="Lihat Hasil"/>
            </form>
        </div>
    </div>
</body>

</html>