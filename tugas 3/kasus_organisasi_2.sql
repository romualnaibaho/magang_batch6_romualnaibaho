CREATE TABLE employee
(
id int,
nama varchar(255),
atasan_id int,
company_id int
);

CREATE TABLE company
(
id int,
nama varchar(255),
alamat varchar(255)
);

INSERT INTO employee VALUES
(1, 'Pak Budi', null, 1),
(2, 'Pak Tono', 1, 1),
(3, 'Pak Totok', 1, 1),
(4, 'Bu Sinta', 2, 1),
(5, 'Bu Novi', 3, 1),
(6, 'Andre', 3, 1),
(7, 'Dono', 4, 1),
(8, 'Ismir', 5, 1),
(9, 'Anto', 5, 1);

INSERT INTO company VALUES
(1, 'PT JAVAN', 'Sleman'),
(2, 'PT Dicoding', 'Bandung');

-- --------------------------------------------------------
-- CEO adalah orang yang posisinya tertinggi. Buat query untuk mencari siapa CEO

SELECT * FROM employee
WHERE atasan_id IS null;

-- Staff biasa adalah orang yang tidak punya bawahan. Buat query untuk mencari siapa staff biasa

SELECT * FROM employee
WHERE id NOT IN (SELECT DISTINCT atasan_id FROM employee WHERE atasan_id IS NOT NULL);

-- Direktur adalah orang yang dibawah langsung CEO. Buat query untuk mencari siapa direktur

SELECT * FROM employee
WHERE atasan_id IS (SELECT id FROM employee WHERE atasan_id IS NULL );

-- Manager adalah orang yang dibawah direktur dan di atas staff.  Buat query untuk mencari siapa manager

SELECT * FROM employee
WHERE atasan_id IN (SELECT id FROM employee WHERE atasan_id IS (SELECT id FROM employee WHERE atasan_id IS NULL ));

-- Buat sebuah query untuk mencari jumlah bawahan dengan parameter nama. Contoh kasus:
-- 1) Bawahan pak budi berjumlah 8 orang
SELECT COUNT(id) as Total_Bawahan FROM employee
WHERE atasan_id >= (SELECT id FROM employee WHERE nama = 'Pak Budi');

-- 2) Bawahan Bu Sinta berjumlah 2 orang
SELECT COUNT(id) as Total_Bawahan FROM employee
WHERE atasan_id IS (SELECT id FROM employee WHERE nama = 'Bu Sinta');


