<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name',
    ];

    public function films()
    {
        return $this->belongsToMany('App\Film', 'customer_film', 'customer_id', 'film_id');
    }
}
