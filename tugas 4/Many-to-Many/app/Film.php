<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title',
    ];

    public function customers()
    {
        return $this->belongsToMany('App\Customer', 'customer_film', 'film_id', 'customer_id');
    }
}
