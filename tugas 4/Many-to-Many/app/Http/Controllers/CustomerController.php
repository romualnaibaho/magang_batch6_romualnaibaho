<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Film;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::all();
 
        return $customers->toJson();
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'name' => 'required',
        ]);

        $penonton = Customer::create([
            'name' => $validatedData['name'],
          ]);
 
        $msg = [
            'success' => true,
            'message' => 'Penonton created successfully!'
        ];
 
        return response()->json($msg);
    }
 
    public function getCustomer($id) // for edit and show
    {
        $customer = Customer::find($id);
 
        return $customer->toJson();
    }
 
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'name' => 'required',
        ]);
 
        $customer = Customer::find($id);
        $customer->name = $validatedData['name'];
        $customer->save();
 
        $msg = [
            'success' => true,
            'message' => 'Penonton updated successfully'
        ];
 
        return response()->json($msg);
    }
 
    public function delete($id)
    {
        $customer = Customer::find($id);
        if(!empty($customer)){
            $customer->delete();
            $msg = [
                'success' => true,
                'message' => 'Penonton deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Penonton deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}