import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import CustomerIndex from './CustomerIndex'
import CustomerCreate from './CustomerCreate'
import CustomerShow from './CustomerShow'
import CustomerEdit from './CustomerEdit'
 
class App extends Component {
    render () {
        return (
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                    <Route exact path='/' component={CustomerIndex}/>
                    <Route exact path='/create' component={CustomerCreate} />
                    <Route path='/customer/edit/:id' component={CustomerEdit} />
                    <Route path='/customer/:id' component={CustomerShow} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}
 
ReactDOM.render(<App />, document.getElementById('app'))