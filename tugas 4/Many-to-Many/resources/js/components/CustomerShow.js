import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
 
class CustomerShow extends Component {
    constructor (props) {
    super(props)
    this.state = {
        customer: {}
    }
    }

    componentDidMount () {

    const customerId = this.props.match.params.id

    axios.get(`/api/customer/${customerId}`).then(response => {
        this.setState({
            customer: response.data
        })
    })
    }

    render () {
    const { customer } = this.state

    return (
        <div className='container py-4'>
        <div className='row justify-content-center'>
            <div className='col-md-8'>
            <div className='card'>
                <div className='card-header'>Detail Penonton:</div>
                <div className='card-body'>
                    <p>{customer.name}</p>
                <Link
                    className='btn btn-primary'
                    to={`/`}
                    >Back
                </Link>
                </div>
            </div>
            </div>
        </div>
        </div>
    )
    }
}
 
export default CustomerShow