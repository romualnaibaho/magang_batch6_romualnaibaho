<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/customers', 'CustomerController@index');
Route::post('/customer/store', 'CustomerController@store');
Route::get('/customer/edit/{id}', 'CustomerController@getCustomer');
Route::get('/customer/{id}', 'CustomerController@getCustomer');
Route::put('/customer/{id}', 'CustomerController@update');
Route::delete('/customer/delete/{id}', 'CustomerController@delete');
