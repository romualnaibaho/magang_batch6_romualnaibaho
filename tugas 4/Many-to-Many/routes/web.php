<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/customers', 'app');
Route::view('/customer/edit/{id}', 'app');
Route::view('/customer/{id}', 'app');
Route::view('/', 'app');
Route::view('/{path}', 'app');
