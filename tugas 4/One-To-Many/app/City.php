<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nama', 'province_id',
    ];

    public function province(){
        return $this->belongsTo('App\Province');
    }

    public function subdistricts(){
        return $this->hasMany('App\Sub-District');
    }
}
