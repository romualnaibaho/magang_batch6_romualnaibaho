<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index($id)
    {
        $province = \App\Province::find($id);

        $cities = $province->cities;
 
        return $cities->toJson();
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'id' => 'required',
          'nama' => 'required',
          'province_id' => 'required',
        ]);
 
        $data = \App\City::create([
          'id' => $validatedData['id'],
          'nama' => $validatedData['nama'],
          'province_id' => $validatedData['province_id'],
        ]);
 
        $msg = [
            'success' => true,
            'message' => 'City created successfully!'
        ];
 
        return response()->json($msg);
    }
 
    public function getCity($id) // for edit and show
    {
        $cities = \App\City::find($id);
 
        return $cities->toJson();
    }
 
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'id' => 'required',
            'nama' => 'required',
            'province_id' => 'required',
        ]);
 
        $city = \App\City::find($id);
        $city->id = $validatedData['id'];
        $city->nama = $validatedData['nama'];
        $city->province_id = $validatedData['province_id'];
        $city->save();
 
        $msg = [
            'success' => true,
            'message' => 'City updated successfully'
        ];
 
        return response()->json($msg);
    }
 
    public function delete($id)
    {
        $city = \App\City::find($id);
        if(!empty($city)){
            $city->delete();
            $msg = [
                'success' => true,
                'message' => 'City deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'City deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
