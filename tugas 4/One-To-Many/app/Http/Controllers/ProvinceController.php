<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProvinceController extends Controller
{
    public function index()
    {
        $provinces = \App\Province::all();
 
        return $provinces->toJson();
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'id' => 'required',
          'nama' => 'required',
        ]);
 
        $data = \App\Province::create([
          'id' => $validatedData['id'],
          'nama' => $validatedData['nama'],
        ]);
 
        $msg = [
            'success' => true,
            'message' => 'Province created successfully!'
        ];
 
        return response()->json($msg);
    }
 
    public function getProvince($id) // for edit and show
    {
        $province = \App\Province::find($id);
 
        return $province->toJson();
    }
 
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'id' => 'required',
          'nama' => 'required',
        ]);
 
        $province = \App\Province::find($id);
        $province->id = $validatedData['id'];
        $province->nama = $validatedData['nama'];
        $province->save();
 
        $msg = [
            'success' => true,
            'message' => 'Province updated successfully'
        ];
 
        return response()->json($msg);
    }
 
    public function delete($id)
    {
        $province = \App\Province::find($id);
        if(!empty($province)){
            $province->delete();
            $msg = [
                'success' => true,
                'message' => 'Province deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Province deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
