<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubDistrictController extends Controller
{
    public function index($id)
    {
        $city = \App\City::find($id);

        $subdistricts = $city->subdistricts;
 
        return $subdistricts->toJson();
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'id' => 'required',
          'nama' => 'required',
          'city_id' => 'required',
        ]);
 
        $data = \App\SubDistrict::create([
          'id' => $validatedData['id'],
          'nama' => $validatedData['nama'],
          'city_id' => $validatedData['city_id'],
        ]);
 
        $msg = [
            'success' => true,
            'message' => 'Sub-District created successfully!'
        ];
 
        return response()->json($msg);
    }
 
    public function getSubDistrict($id) // for edit and show
    {
        $subdistrict = \App\SubDistrict::find($id);
 
        return $subdistrict->toJson();
    }
 
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'id' => 'required',
            'nama' => 'required',
            'city_id' => 'required',
        ]);
 
        $subdistrict = \App\SubDistrict::find($id);
        $subdistrict->id = $validatedData['id'];
        $subdistrict->nama = $validatedData['nama'];
        $subdistrict->city_id = $validatedData['city_id'];
        $subdistrict->save();
 
        $msg = [
            'success' => true,
            'message' => 'Sub-District updated successfully'
        ];
 
        return response()->json($msg);
    }
 
    public function delete($id)
    {
        $subdistrict = \App\SubDistrict::find($id);
        if(!empty($subdistrict)){
            $subdistrict->delete();
            $msg = [
                'success' => true,
                'message' => 'Sub-District deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Sub-District deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
