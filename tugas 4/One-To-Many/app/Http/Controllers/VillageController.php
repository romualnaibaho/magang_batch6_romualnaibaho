<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VillageController extends Controller
{
    public function index($id)
    {
        $subdistrict = \App\SubDistrict::find($id);

        $village = $subdistrict->villages;
 
        return $village->toJson();
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'id' => 'required',
          'nama' => 'required',
          'subdistrict_id' => 'required',
        ]);
 
        $data = \App\Village::create([
          'id' => $validatedData['id'],
          'nama' => $validatedData['nama'],
          'subdistrict_id' => $validatedData['subdistrict_id'],
        ]);
 
        $msg = [
            'success' => true,
            'message' => 'Village created successfully!'
        ];
 
        return response()->json($msg);
    }
 
    public function getVillage($id) // for edit and show
    {
        $village = \App\Village::find($id);
 
        return $village->toJson();
    }
 
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'id' => 'required',
            'nama' => 'required',
            'subdistrict_id' => 'required',
        ]);
 
        $village = \App\Village::find($id);
        $village->id = $validatedData['id'];
        $village->nama = $validatedData['nama'];
        $village->subdistrict_id = $validatedData['subdistrict_id'];
        $village->save();
 
        $msg = [
            'success' => true,
            'message' => 'Village updated successfully'
        ];
 
        return response()->json($msg);
    }
 
    public function delete($id)
    {
        $village = \App\Village::find($id);
        if(!empty($village)){
            $village->delete();
            $msg = [
                'success' => true,
                'message' => 'Village deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Village deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
