<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubDistrict extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nama', 'city_id',
    ];

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function villages(){
        return $this->hasMany('App\Village');
    }
}
