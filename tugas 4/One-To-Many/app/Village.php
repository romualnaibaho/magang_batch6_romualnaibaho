<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nama', 'subdistrict_id',
    ];

    public function subdistrict(){
        return $this->belongsTo('App\Sub-District');
    }
}
