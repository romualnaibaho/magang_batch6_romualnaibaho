import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import ProvinceIndex from './ProvinceIndex'
import ProvinceCreate from './ProvinceCreate'
import ProvinceShow from './ProvinceShow'
import ProvinceEdit from './ProvinceEdit'
 
class App extends Component {
    render () {
        return (
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                    <Route exact path='/' component={ProvinceIndex}/>
                    <Route exact path='/create' component={ProvinceCreate} />
                    <Route path='/province/edit/:id' component={ProvinceEdit} />
                    <Route path='/province/:id' component={ProvinceShow} />

                    <Route exact path='/' component={CityIndex}/>
                    <Route exact path='/city/create' component={CityCreate} />
                    <Route path='/city/edit/:id' component={CityEdit} />
                    <Route path='/city/:id' component={CityShow} />

                    <Route exact path='/' component={SubDistrictIndex}/>
                    <Route exact path='/subdistrict/create' component={SubDistrictCreate} />
                    <Route path='/subdistrict/edit/:id' component={SubDistrictEdit} />
                    <Route path='/subdistrict/:id' component={SubDistrictShow} />

                    <Route exact path='/' component={VillageIndex}/>
                    <Route exact path='/village/create' component={VillageCreate} />
                    <Route path='/village/edit/:id' component={VillageEdit} />
                    <Route path='/village/:id' component={VillageShow} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}
 
ReactDOM.render(<App />, document.getElementById('app'))