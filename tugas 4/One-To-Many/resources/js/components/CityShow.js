import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
 
class CityShow extends Component {
    constructor (props) {
    super(props)
    this.state = {
        city: {}
    }
    }

    componentDidMount () {

    const cityId = this.props.match.params.id

    axios.get(`/api/city/${cityId}`).then(response => {
        this.setState({
            city: response.data
        })
    })
    }

    render () {
    const { city } = this.state

    return (
        <div className='container py-4'>
        <div className='row justify-content-center'>
            <div className='col-md-8'>
            <div className='card'>
                <div className='card-header'>Nama Kabupaten/Kota: {city.nama}</div>
                <div className='card-body'>
                <Link
                    className='btn btn-primary'
                    to={`/`}
                    >Back
                </Link>
                </div>
            </div>
            </div>
        </div>
        </div>
    )
    }
}
 
export default CityShow