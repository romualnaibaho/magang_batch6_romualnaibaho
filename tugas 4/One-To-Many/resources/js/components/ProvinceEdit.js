import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';
 
class ProvinceEdit extends Component {
    constructor (props) {
    super(props)
    this.state = {
        id:'',
        nama: '',
        alert: null,
        message:'',
        errors: []
    }
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.handleUpdateProvince = this.handleUpdateProvince.bind(this)
    this.hasErrorFor = this.hasErrorFor.bind(this)
    this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
    this.setState({
        [event.target.name]: event.target.value
    })
    }

    componentDidMount () {

    const provinceId = this.props.match.params.id

    axios.get(`/api/province/edit/${provinceId}`).then(response => {
            this.setState({
                nama: response.data.nama,
            })
        })
    }

    goToHome(){
    const getAlert = () => (
        <SweetAlert
            success
            title="Success!"
            onConfirm={() => this.onSuccess() }
            onCancel={this.hideAlert()}
            timeout={2000}
            confirmBtnText="Oke"
            >
            {this.state.message}
        </SweetAlert>
        );
        this.setState({
        alert: getAlert()
        });
    }

    onSuccess() {
    this.props.history.push('/');
    }

    hideAlert() {
    this.setState({
        alert: null
    });
    }

    handleUpdateProvince (event) {
    event.preventDefault()

    const province = {
        nama: this.state.nama,
    }

    const provinceId = this.props.match.params.id

    axios.put(`/api/province/${provinceId}`, province)
        .then(response => {
        // redirect to the homepage
        var msg = response.data.success;
        if(msg == true){
            this.setState({
                message: response.data.message
            })
            return this.goToHome();
        }

        });
    }

    hasErrorFor (field) {
    return !!this.state.errors[field]
    }

    renderErrorFor (field) {
    if (this.hasErrorFor(field)) {
        return (
        <span className='invalid-feedback'>
            <strong>{this.state.errors[field][0]}</strong>
        </span>
        )
    }
    }

    render () {
    const { province } = this.state
    return (
        <div className='container py-4'>
        <div className='row justify-content-center'>
            <div className='col-md-6'>
            <div className='card'>
                <div className='card-header'>Create new province</div>
                <div className='card-body'>
                <form onSubmit={this.handleUpdateProvince}>
                    <div className='form-group'>
                    <label htmlFor='title'>Nama</label>
                    <input
                        id='nama'
                        type='text'
                        className={`form-control ${this.hasErrorFor('nama') ? 'is-invalid' : ''}`}
                        name='nama'
                        value={this.state.nama}
                        onChange={this.handleFieldChange}
                    />
                    {this.renderErrorFor('nama')}
                    </div>
                    <Link
                    className='btn btn-secondary'
                    to={`/`}
                    >Back
                    </Link>
                    <button className='btn btn-primary'>Update</button>
                    {this.state.alert}
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    )
    }
}
export default ProvinceEdit