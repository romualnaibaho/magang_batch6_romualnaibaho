import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
 
class ProvinceShow extends Component {
    constructor (props) {
    super(props)
    this.state = {
        province: {}
    }
    }

    componentDidMount () {

    const provinceId = this.props.match.params.id

    axios.get(`/api/province/${provinceId}`).then(response => {
        this.setState({
            province: response.data
        })
    })
    }

    render () {
    const { province } = this.state

    return (
        <div className='container py-4'>
        <div className='row justify-content-center'>
            <div className='col-md-8'>
            <div className='card'>
                <div className='card-header'>Nama Provinsi: {province.nama}</div>
                <div className='card-body'>
                <Link
                    className='btn btn-primary'
                    to={`/`}
                    >Back
                </Link>
                </div>
            </div>
            </div>
        </div>
        </div>
    )
    }
}
 
export default ProvinceShow