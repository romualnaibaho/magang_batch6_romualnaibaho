import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';
 
class SubDistrictEdit extends Component {
    constructor (props) {
    super(props)
    this.state = {
        id:'',
        nama: '',
        city_id: '',
        alert: null,
        message:'',
        errors: []
    }
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.handleUpdateSubDistrict = this.handleUpdateSubDistrict.bind(this)
    this.hasErrorFor = this.hasErrorFor.bind(this)
    this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
    this.setState({
        [event.target.name]: event.target.value
    })
    }

    componentDidMount () {

    const subdistrictId = this.props.match.params.id

    axios.get(`/api/subdistrict/edit/${subdistrictId}`).then(response => {
            this.setState({
                nama: response.data.nama,
                city_id: response.data.city_id,
            })
        })
    }

    goToHome(){
    const getAlert = () => (
        <SweetAlert
            success
            title="Success!"
            onConfirm={() => this.onSuccess() }
            onCancel={this.hideAlert()}
            timeout={2000}
            confirmBtnText="Oke"
            >
            {this.state.message}
        </SweetAlert>
        );
        this.setState({
        alert: getAlert()
        });
    }

    onSuccess() {
    this.props.history.push('/');
    }

    hideAlert() {
    this.setState({
        alert: null
    });
    }

    handleUpdateSubDistrict (event) {
    event.preventDefault()

    const subdistrict = {
        nama: this.state.nama,
        city_id: this.state.city_id,
    }

    const subdistrictId = this.props.match.params.id

    axios.put(`/api/subdistrict/${subdistrictId}`, subdistrict)
        .then(response => {
        // redirect to the homepage
        var msg = response.data.success;
        if(msg == true){
            this.setState({
                message: response.data.message
            })
            return this.goToHome();
        }

        });
    }

    hasErrorFor (field) {
    return !!this.state.errors[field]
    }

    renderErrorFor (field) {
    if (this.hasErrorFor(field)) {
        return (
        <span className='invalid-feedback'>
            <strong>{this.state.errors[field][0]}</strong>
        </span>
        )
    }
    }

    render () {
    const { subdistrict } = this.state
    return (
        <div className='container py-4'>
        <div className='row justify-content-center'>
            <div className='col-md-6'>
            <div className='card'>
                <div className='card-header'>Create new Sub-District</div>
                <div className='card-body'>
                <form onSubmit={this.handleUpdateSubDistrict}>
                    <div className='form-group'>
                    <label htmlFor='title'>Nama</label>
                    <input
                        id='nama'
                        type='text'
                        className={`form-control ${this.hasErrorFor('nama') ? 'is-invalid' : ''}`}
                        name='nama'
                        value={this.state.nama}
                        onChange={this.handleFieldChange}
                    />
                    {this.renderErrorFor('nama')}
                    </div>
                    <div className='form-group'>
                        <input
                          id='city_id'
                          type='number'
                          className={`form-control ${this.hasErrorFor('city_id') ? 'is-invalid' : ''}`}
                          name='city_id'
                          value={this.state.city_id}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('city_id')}
                      </div>
                    <Link
                    className='btn btn-secondary'
                    to={`/`}
                    >Back
                    </Link>
                    <button className='btn btn-primary'>Update</button>
                    {this.state.alert}
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    )
    }
}
export default SubDistrictEdit