import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
 
class SubDistrictShow extends Component {
    constructor (props) {
    super(props)
    this.state = {
        subdistrict: {}
    }
    }

    componentDidMount () {

    const subdistrictId = this.props.match.params.id

    axios.get(`/api/subdistrict/${subdistrictId}`).then(response => {
        this.setState({
            subdistrict: response.data
        })
    })
    }

    render () {
    const { subdistrict } = this.state

    return (
        <div className='container py-4'>
        <div className='row justify-content-center'>
            <div className='col-md-8'>
            <div className='card'>
                <div className='card-header'>Nama Kecamatan: {subdistrict.nama}</div>
                <div className='card-body'>
                <Link
                    className='btn btn-primary'
                    to={`/`}
                    >Back
                </Link>
                </div>
            </div>
            </div>
        </div>
        </div>
    )
    }
}
 
export default SubDistrictShow