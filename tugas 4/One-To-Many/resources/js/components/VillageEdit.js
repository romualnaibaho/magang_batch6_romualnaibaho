import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';
 
class VillageEdit extends Component {
    constructor (props) {
    super(props)
    this.state = {
        id:'',
        nama: '',
        subdistrict_id: '',
        alert: null,
        message:'',
        errors: []
    }
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.handleUpdateVillage = this.handleUpdateVillage.bind(this)
    this.hasErrorFor = this.hasErrorFor.bind(this)
    this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (event) {
    this.setState({
        [event.target.name]: event.target.value
    })
    }

    componentDidMount () {

    const villageId = this.props.match.params.id

    axios.get(`/api/village/edit/${villageId}`).then(response => {
            this.setState({
                nama: response.data.nama,
                subdistrict_id: response.data.subdistrict_id,
            })
        })
    }

    goToHome(){
    const getAlert = () => (
        <SweetAlert
            success
            title="Success!"
            onConfirm={() => this.onSuccess() }
            onCancel={this.hideAlert()}
            timeout={2000}
            confirmBtnText="Oke"
            >
            {this.state.message}
        </SweetAlert>
        );
        this.setState({
        alert: getAlert()
        });
    }

    onSuccess() {
    this.props.history.push('/');
    }

    hideAlert() {
    this.setState({
        alert: null
    });
    }

    handleUpdateVillage (event) {
    event.preventDefault()

    const village = {
        nama: this.state.nama,
        subdistrict_id: this.state.subdistrict_id,
    }

    const villageId = this.props.match.params.id

    axios.put(`/api/village/${villageId}`, village)
        .then(response => {
        // redirect to the homepage
        var msg = response.data.success;
        if(msg == true){
            this.setState({
                message: response.data.message
            })
            return this.goToHome();
        }

        });
    }

    hasErrorFor (field) {
    return !!this.state.errors[field]
    }

    renderErrorFor (field) {
    if (this.hasErrorFor(field)) {
        return (
        <span className='invalid-feedback'>
            <strong>{this.state.errors[field][0]}</strong>
        </span>
        )
    }
    }

    render () {
    const { village } = this.state
    return (
        <div className='container py-4'>
        <div className='row justify-content-center'>
            <div className='col-md-6'>
            <div className='card'>
                <div className='card-header'>Create new village</div>
                <div className='card-body'>
                <form onSubmit={this.handleUpdateVillage}>
                    <div className='form-group'>
                    <label htmlFor='title'>Nama</label>
                    <input
                        id='nama'
                        type='text'
                        className={`form-control ${this.hasErrorFor('nama') ? 'is-invalid' : ''}`}
                        name='nama'
                        value={this.state.nama}
                        onChange={this.handleFieldChange}
                    />
                    {this.renderErrorFor('nama')}
                    </div>
                    <div className='form-group'>
                        <input
                          id='subdistrict_id'
                          type='number'
                          className={`form-control ${this.hasErrorFor('subdistrict_id') ? 'is-invalid' : ''}`}
                          name='subdistrict_id'
                          value={this.state.subdistrict_id}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('subdistrict_id')}
                      </div>
                    <Link
                    className='btn btn-secondary'
                    to={`/`}
                    >Back
                    </Link>
                    <button className='btn btn-primary'>Update</button>
                    {this.state.alert}
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    )
    }
}
export default VillageEdit