import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
 
class VillageShow extends Component {
    constructor (props) {
    super(props)
    this.state = {
        village: {}
    }
    }

    componentDidMount () {

    const villageId = this.props.match.params.id

    axios.get(`/api/village/${villageId}`).then(response => {
        this.setState({
            village: response.data
        })
    })
    }

    render () {
    const { village } = this.state

    return (
        <div className='container py-4'>
        <div className='row justify-content-center'>
            <div className='col-md-8'>
            <div className='card'>
                <div className='card-header'>Nama Desa: {village.nama}</div>
                <div className='card-body'>
                <Link
                    className='btn btn-primary'
                    to={`/`}
                    >Back
                </Link>
                </div>
            </div>
            </div>
        </div>
        </div>
    )
    }
}
 
export default VillageShow