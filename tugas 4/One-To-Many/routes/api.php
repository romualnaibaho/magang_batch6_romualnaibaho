<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/provinces', 'ProvinceController@index');
Route::post('/province/store', 'ProvinceController@store');
Route::get('/province/edit/{id}', 'ProvinceController@getProvince');
Route::get('/province/{id}', 'ProvinceController@getProvince');
Route::put('/province/{id}', 'ProvinceController@update');
Route::delete('/province/delete/{id}', 'ProvinceController@delete');

Route::get('/cities/{id}', 'CityController@index');
Route::post('/city/store', 'CityController@store');
Route::get('/city/edit/{id}', 'CityController@getCity');
Route::get('/city/{id}', 'CityController@getCity');
Route::put('/city/{id}', 'CityController@update');
Route::delete('/city/delete/{id}', 'CityController@delete');

Route::get('/subdistricts/{id}', 'SubDistrictController@index');
Route::post('/subdistrict/store', 'SubDistrictController@store');
Route::get('/subdistrict/edit/{id}', 'SubDistrictController@getSubDistrict');
Route::get('/subdistrict/{id}', 'SubDistrictController@getSubDistrict');
Route::put('/subdistrict/{id}', 'SubDistrictController@update');
Route::delete('/subdistrict/delete/{id}', 'SubDistrictController@delete');

Route::get('/villages/{id}', 'VillageController@index');
Route::post('/village/store', 'VillageController@store');
Route::get('/village/edit/{id}', 'VillageController@getSubDistrict');
Route::get('/village/{id}', 'VillageController@getSubDistrict');
Route::put('/village/{id}', 'VillageController@update');
Route::delete('/village/delete/{id}', 'VillageController@delete');