<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/provinces', 'app');
Route::view('/province/edit/{id}', 'app');
Route::view('/province/{id}', 'app');
Route::view('/', 'app');

Route::view('/city', 'app');
Route::view('/city/edit/{id}', 'app');
Route::view('/city/{id}', 'app');
Route::view('/', 'app');

Route::view('/subdistrict', 'app');
Route::view('/subdistrict/edit/{id}', 'app');
Route::view('/subdistrict/{id}', 'app');
Route::view('/', 'app');

Route::view('/village', 'app');
Route::view('/village/edit/{id}', 'app');
Route::view('/village/{id}', 'app');
Route::view('/', 'app');
Route::view('/{path}', 'app');
