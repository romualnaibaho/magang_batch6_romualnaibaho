<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompanyController extends Controller
{

    public function index(){
        
        $company = Company::orderBy('id', 'asc')->get();
        
        return view('company', compact('company'));

    }

    public function add_company(){

        return view('add-company');
    }

    public function save_company(Request $request){

        Company::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat
        ]);

        return redirect()->route('company-index');
    }

    public function update_company($id){

        $company = Company::where('id', $id)->get();
        
        return view('update-company', compact('company'));
    }

    public function save_updated_company(Request $request){

        Company::where('id', $request->id)
            ->update([
                'nama' => $request->nama,
                'alamat' => $request->alamat
            ]);

        return redirect()->route('company-index');
    }

    public function delete_company($id){

        Company::where('id', $id)->delete();
        
        return redirect()->route('company-index');
    }
}
