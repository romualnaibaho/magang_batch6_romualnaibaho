<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/styleku.css')}}">

    <title>SPM</title>
</head>
<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8 col-md-4 form">

                <div class="text-center mb-4">
                    <h4>Tambah Data Employee</h4>
                </div>

                <form method="POST" action="{{ route('save-data') }}">
                @csrf
                    <div class="form-group">
						<label>Nama: </label>
						<input type="text" name="nama" class="form-control" required="required"/>
					</div>
                    <div class="form-group">
						<label>Atasan ID: </label>
						<input type="number" name="atasan_id" class="form-control"/>
					</div>
                    <div class="form-group">
						<label>Departemen ID: </label>
						<input type="number" name="departemen_id" class="form-control" required="required"/>
					</div>
                    <div class="form-group text-center mt-4">
						<button name="submit" class="btn btn-primary"><span class = "glyphicon glyphicon-plus"></span> Tambahkan Data</button>
					</div>
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>