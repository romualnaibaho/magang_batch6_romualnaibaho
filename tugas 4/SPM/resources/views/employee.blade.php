<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/styleku.css')}}">

    <title>SPM</title>
</head>
<body>
    
    <div class="container">
        <div class="title text-center">
            <h2>Employees</h2>
        </div>

        <div class="add-btn d-flex justify-content-end">
            <a href="{{ route('add-employee') }}"><button name="add" class = "btn btn-primary"><span class = "glyphicon glyphicon-plus"></span>+ Add Employee</button></a>
        </div>

        <div class="data">
            <table class="table table-bordered alert-warning table-hover">
                <thead>
                    <tr class="text-center">
                        <th scope="col">No.</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Atasan ID</th>
                        <th scope="col">Company ID</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $number = 1;
                    ?>
                    @forelse ($employee as $employee)
                    <tr>
                        <th class="text-center" scope="row">{{$number}}</th>
                        <td>{{$employee->nama}}</td>
                        <td class="text-center">{{$employee->atasan_id}}</td>
                        <td class="text-center">{{$employee->departemen_id}}</td>
                        <td><center>
                            <a href="{{ route('update-data', $employee->id) }}" class = "btn btn-warning"><span class = "glyphicon glyphicon-edit"></span> Update</a> | 
                            <a href="{{ route('delete-data', $employee->id) }}" class = "btn btn-danger"><span class = "glyphicon glyphicon-trash"></span> Delete</a>
                            </center>
                        </td>
                    </tr>
                    <?php
                        $number++;
                    ?>
                    @empty
                        <td class="text-center" colspan="5">No Employee added !</p>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="add-btn d-flex justify-content-left">
            <a href="{{ route('index') }}"><button name="add" class = "btn btn-secondary"><span class = "glyphicon glyphicon-plus"></span>< Kembali</button></a>
        </div>

    </div>

</body>
</html>