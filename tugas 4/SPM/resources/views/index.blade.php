<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/styleku.css')}}">

    <title>SPM</title>
</head>
<body>
    
    <div class="container">
        <div class="row justify-content-center menu">
            <div class="col 6 text-center">
                <a href="{{ route('employee-index') }}"><button type="button" class="myBtn btn btn-outline-primary">Employee</button></a>
            </div>
            <div class="col 6 text-center">
                <a href="{{ route('company-index') }}"><button type="button" class="myBtn btn btn-outline-primary">Company</button></a>
            </div>
        </div>
    </div>
</body>
</html>