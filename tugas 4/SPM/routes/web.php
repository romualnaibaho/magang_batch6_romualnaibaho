<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('index');
})->name('index');

Route::get('/employee', 'EmployeeController@index')->name('employee-index');

Route::get('/add-employee', 'EmployeeController@add_employee')->name('add-employee');

Route::post('/save-employee', 'EmployeeController@save_employee')->name('save-data');

Route::get('/update-employee/{id}', 'EmployeeController@update_employee')->name('update-data');

Route::put('/save-update-employee', 'EmployeeController@save_updated_employee')->name('save-updated-data');

Route::get('/delete-employee/{id}', 'EmployeeController@delete_employee')->name('delete-data');

Route::get('/company', 'CompanyController@index')->name('company-index');

Route::get('/add-company', 'CompanyController@add_company')->name('add-company');

Route::post('/save-company', 'CompanyController@save_company')->name('save-company-data');

Route::get('/update-company/{id}', 'CompanyController@update_company')->name('update-company-data');

Route::put('/save-update-company', 'CompanyController@save_updated_company')->name('save-updated-company-data');

Route::get('/delete-company/{id}', 'CompanyController@delete_company')->name('delete-company-data');
