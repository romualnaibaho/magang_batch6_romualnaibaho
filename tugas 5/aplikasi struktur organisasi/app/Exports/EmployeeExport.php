<?php

namespace App\Exports;

use App\Employee;
use App\Company;
use Maatwebsite\Excel\Concerns\FromCollection;

class EmployeeExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {  
        return Company::join('employees', 'employees.departemen_id', '=', 'companies.id')
        ->get(['employees.id', 'employees.nama', 'employees.jabatan','companies.nama_company']);
    }
}
