<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;

class EmployeeController extends Controller
{
    public function index(){
        
        $employee = Employee::orderBy('id', 'asc')->get();
        
        return view('employee', compact('employee'));

    }

    public function add_employee(){

        return view('add-employee');
    }

    public function save_employee(Request $request){

        Employee::create([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan_id,
            'departemen_id' => $request->departemen_id
        ]);

        return redirect()->route('employee-index');
    }

    public function update_employee($id){

        $employee = Employee::where('id', $id)->get();
        
        return view('update-employee', compact('employee'));
    }

    public function save_updated_employee(Request $request){

        Employee::where('id', $request->id)
            ->update([
                'nama' => $request->nama,
                'atasan_id' => $request->atasan_id,
                'departemen_id' => $request->departemen_id
            ]);

        return redirect()->route('employee-index');
    }

    public function delete_employee($id){

        Employee::where('id', $id)->delete();
        
        return redirect()->route('employee-index');
    }
}
