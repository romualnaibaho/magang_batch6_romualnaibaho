<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\EmployeeExport;
use PDF;

class PrintController extends Controller
{
    public function cetak_pdf(){

        $employee = Company::join('employees', 'employees.departemen_id', '=', 'companies.id')
                    ->get();
 
    	$pdf = PDF::loadview('struktur-organisasi',['employee'=>$employee]);
    	return $pdf->download('struktur-organisasi');
    }

    public function export_excel()
	{
		return Excel::download(new EmployeeExport, 'struktur-organisasi.xlsx');
	}
}
