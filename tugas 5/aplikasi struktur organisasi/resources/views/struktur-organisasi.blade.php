<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/styleku.css')}}">

    <title>SPM</title>
</head>
<body>
    
    <div">
        <div class="title text-center">
            <h2>Struktur Organisasi</h2>
        </div>

        <div class="data">
            <table class="table table-bordered alert-warning">
                <thead>
                    <tr class="text-center">
                        <th scope="col">ID.</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Posisi</th>
                        <th scope="col">Perusahaan</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($employee as $employee)
                    <tr>
                        <th class="text-center" scope="row">{{$employee->id}}</th>
                        <td>{{$employee->nama}}</td>
                        <td class="text-center">{{$employee->jabatan}}</td>
                        <td class="text-center">{{$employee->nama_company}}</td>
                    </tr>
                    @empty
                    <tr>
                        <th colspan="4">No Data Entry Yet !</th>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>