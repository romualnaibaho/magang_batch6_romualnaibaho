-- Jumlah komplain setiap bulan
SELECT "Date Received", COUNT("Complaint ID") as Total_Complaint from ConsumerComplaints
GROUP BY strftime('%m', "Date Received");

-- Komplain yang memiliki tags ‘Older American’
SELECT * FROM ConsumerComplaints
WHERE Tags LIKE '%Older American%';

--Buat sebuah view yang menampilkan data nama perusahaan, jumlah company response to consumer
SELECT
       Company,
       COUNT(CASE WHEN "Company Response to Consumer" IS 'Closed' then 1 else null end) as Closed,
       COUNT(CASE WHEN "Company Response to Consumer" IS 'Closed with explanation' then 1 else null end) as Closed_With_Explanation,
       COUNT(CASE WHEN "Company Response to Consumer" IS 'Closed with non-monetary relief' then 1 else null end) as Closed_with_non_monetary_relief
from ConsumerComplaints
GROUP BY Company;