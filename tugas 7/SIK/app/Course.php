<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'courses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'course_id', 'nama_mk', 'SKS',
    ];

    public function teachers(){
        return $this->belongsToMany('App\Teacher');
    }

    public function students(){
        return $this->belongsToMany('App\Student', 'classrooms', 'course_id', 'student_id');
    }
}
