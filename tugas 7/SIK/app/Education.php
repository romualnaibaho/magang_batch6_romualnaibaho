<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'educations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'strata', 'jurusan', 'sekolah', 'tahun_mulai', 'tahun_selesai',
    ];

    public function teachers(){
        return $this->belongsToMany('App\Teacher');
    }
}
