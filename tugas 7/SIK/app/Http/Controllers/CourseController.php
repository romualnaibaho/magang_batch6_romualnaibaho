<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

class CourseController extends Controller
{
    public function show_mata_kuliah(){

        $course = Course::get();

        return view('course.course', compact('course'));
    }

    public function add_mata_kuliah(){

        return view('course.add');
    }

    public function save_mata_kuliah(Request $request){

        Course::create([
            'nama_mk' => $request->nama,
            'SKS' => $request->sks,
        ]);

        return redirect()->route('show.mk');
    }

    public function update_mata_kuliah($id){

        $course = Course::where('id', $id)->first();

        return view('course.update', compact('course'));
    }

    public function save_update_mata_kuliah(Request $request){

        Course::where('id', $request->id)->update([
            'nama_mk' => $request->nama,
            'SKS' => $request->sks,
        ]);
        
        return redirect()->route('show.mk');
    }

    public function delete_mata_kuliah($id){

        $course = Course::where('id', $id)->first();

        $course->students()->detach();

        $course->delete();

        return redirect()->route('show.mk');
    }
}
