<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Education;

class EducationController extends Controller
{
    public function show_pendidikan(){

        $pendidikan = Education::get();

        return view('pendidikan.pendidikan', compact('pendidikan'));
    }

    public function add_pendidikan(){

        return view('pendidikan.add');
    }

    public function save_pendidikan(Request $request){

        Education::create([
            'strata' => $request->strata,
            'jurusan' => $request->jurusan,
            'sekolah' => $request->sekolah,
            'tahun_mulai' => $request->tahun_mulai,
            'tahun_selesai' => $request->tahun_selesai,
        ]);

        return redirect()->route('show.pendidikan');
    }

    public function update_pendidikan($id){

        $pendidikan = Education::where('id', $id)->first();

        return view('pendidikan.update', compact('pendidikan'));
    }

    public function save_update_pendidikan(Request $request){

        Education::where('id', $request->id)->update([
            'strata' => $request->strata,
            'jurusan' => $request->jurusan,
            'sekolah' => $request->sekolah,
            'tahun_mulai' => $request->tahun_mulai,
            'tahun_selesai' => $request->tahun_selesai,
        ]);
        
        return redirect()->route('show.pendidikan');
    }

    public function delete_pendidikan($id){

        $education = Education::where('id', $id)->first();

        $education->teachers()->detach();

        $education->delete();

        return redirect()->route('show.pendidikan');
    }
}
