<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Course;

class StudentController extends Controller
{
    public function show_mahasiswa(){

        $mahasiswa = Student::get();

        return view('mahasiswa.mahasiswa', compact('mahasiswa'));
    }

    public function add_mahasiswa(){

        return view('mahasiswa.add');
    }

    public function save_mahasiswa(Request $request){
   
          Student::create([
            'nama_mhs' => $request->nama,
            'nim' => $request->nim,
            'jenis_kelamin' => $request->jenis_kelamin,
            'ttl' => $request->ttl,
          ]);

          return redirect()->route('show.mahasiswa');
    }

    public function update_mahasiswa($id){

        $mahasiswa = Student::where('id', $id)->first();

        return view('mahasiswa.update', compact('mahasiswa'));
    }

    public function save_update_mahasiswa(Request $request){

        Student::where('id', $request->id)->update([
            'nama_mhs' => $request->nama,
            'nim' => $request->nim,
            'jenis_kelamin' => $request->jenis_kelamin,
            'ttl' => $request->ttl,
        ]);
        
        return redirect()->route('show.mahasiswa');
    }

    public function mata_kuliah($id){

        $mahasiswa = Student::where('id', $id)->first();

        $course = $mahasiswa->courses;

        return view('mahasiswa.course', compact('mahasiswa', 'course'));
    }

    public function add_mata_kuliah($id){

        $mahasiswa = Student::where('id', $id)->first();

        $matkuls = $mahasiswa->courses;

        $mkIds = [];

        foreach($matkuls as $matkul){
            $mkIds [] = $matkul->id;
        }

        $course = Course::whereNotIn('id', $mkIds)->get();

        if(count($course) != 0){
            $lastIdx = $course[count($course)-1]->id;
        }else{
            $lastIdx = 0;
        }

        return view('mahasiswa.addCourse', compact('mahasiswa', 'course', 'lastIdx'));

    }

    public function save_mata_kuliah(Request $request){

        $mahasiswa = Student::where('id', $request->id)->first();

        $myCourse = $mahasiswa->courses;

        $sum = 0;

        foreach($myCourse as $mk){
            $sum += $mk->SKS;
        }

        $sum += $request->sks;

        if($sum <= 24){
            $course = Course::create([
                'nama_mk' => $request->nama,
                'SKS' => $request->sks,
            ]);
    
            $mahasiswa->courses()->attach($course);
        }else{
            return redirect()->back()->with('alert', 'SKS Berlebih ! Kurangi pemilihan SKS menjadi kurang dari atau sama dengan 24');
        }

        return redirect()->route('detail.mk', $mahasiswa->id);
    }

    public function save_mata_kuliah_2(Request $request){

        $mahasiswa = Student::where('id', $request->id)->first();

        $myCourse = $mahasiswa->courses;

        $sum = 0;

        foreach($myCourse as $mk){
            $sum += $mk->SKS;
        }

        $courses = $request->input('courses');

        $mkIds = [];

        foreach($courses as $course){
            $mkIds [] = $course;
            $sks = Course::select('SKS')->where('id', $course)->first();
            $sum += $sks->SKS;
        }

        if($sum <= 24){
            $mahasiswa = Student::where('id', $request->id)->first();

            $mahasiswa->courses()->attach($mkIds);
        }else{
            return redirect()->back()->with('alert', 'SKS Berlebih ! Kurangi pemilihan SKS menjadi kurang dari atau sama dengan 24');
        }

        return redirect()->route('detail.mk', $mahasiswa->id);
    }

    public function delete_mahasiswa($id){

        $mahasiswa = Student::where('id', $id)->first();

        $mahasiswa->courses()->detach();

        $mahasiswa->delete();

        return redirect()->route('show.mahasiswa');
    }

    public function remove_course($id){

        $course = Course::where('id', $id)->first();

        $course->students()->detach();

        return redirect()->back();

    }
}
