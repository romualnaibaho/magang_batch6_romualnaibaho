<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use App\Education;
use App\Course;

class TeacherController extends Controller
{
    public function show_dosen(){

        $dosen = Teacher::get();

        return view('dosen.dosen', compact('dosen'));
    }

    public function add_dosen(){

        return view('dosen.add');
    }

    public function save_dosen(Request $request){

        Teacher::create([
            'nama_dsn' => $request->nama,
            'NIP' => $request->NIP,
            'gelar' => $request->gelar,
        ]);

        return redirect()->route('show.dosen');
    }

    public function update_dosen($id){

        $dosen = Teacher::where('id', $id)->first();

        return view('dosen.update', compact('dosen'));
    }

    public function save_update_dosen(Request $request){

        Teacher::where('id', $request->id)->update([
            'nama_dsn' => $request->nama,
            'NIP' => $request->NIP,
            'gelar' => $request->gelar,
        ]);
        
        return redirect()->route('show.dosen');
    }

    public function pendidikan($id){

        $teacher = Teacher::where('id', $id)->first();

        $pendidikan = $teacher->educations;

        return view('dosen.pendidikan', compact('teacher', 'pendidikan'));
    }

    public function add_pendidikan($id){

        $teacher = Teacher::where('id', $id)->first();

        $pendidikans = $teacher->educations;

        $eduIds = [];

        foreach($pendidikans as $pendidikan){
            $eduIds [] = $pendidikan->id;
        }

        $pendidikan = Education::whereNotIn('id', $eduIds)->get();

        if(count($pendidikan) != 0){
            $lastIdx = $pendidikan[count($pendidikan)-1]->id;
        }else{
            $lastIdx = 0;
        }

        return view('dosen.addPendidikan', compact('teacher', 'pendidikan', 'lastIdx'));

    }

    public function delete_dosen($id){

        $dosen = Teacher::where('id', $id)->first();

        $dosen->educations()->detach();

        $dosen->delete();

        return redirect()->route('show.dosen');
    }

    public function save_pendidikan(Request $request){

        $education = Education::create([
            'strata' => $request->strata,
            'jurusan' => $request->jurusan,
            'sekolah' => $request->sekolah,
            'tahun_mulai' => $request->tahun_mulai,
            'tahun_selesai' => $request->tahun_selesai,
        ]);

        $teacher = Teacher::where('id', $request->id)->first();

        $teacher->educations()->attach($education);

        return redirect()->route('detail.pendidikan', $teacher->id);
    }

    public function save_pendidikan_2(Request $request){

        $educations = $request->input('educations');

        $eduIds = [];

        foreach($educations as $education){
            $eduIds [] = $education;
        }

        $teacher = Teacher::where('id', $request->id)->first();

        $teacher->educations()->attach($eduIds);

        return redirect()->route('detail.pendidikan', $teacher->id);
    }

    public function remove_pendidikan($id){

        $education = Education::where('id', $id)->first();

        $education->teachers()->detach();

        return redirect()->back();

    }

    public function course($id){

        $teacher = Teacher::where('id', $id)->first();

        $course = $teacher->courses;

        return view('dosen.course', compact('teacher', 'course'));
    }

    public function add_mata_kuliah($id){

        $dosen = Teacher::where('id', $id)->first();

        $matkuls = $dosen->courses;

        $mkIds = [];

        foreach($matkuls as $matkul){
            $mkIds [] = $matkul->id;
        }

        $course = Course::whereNotIn('id', $mkIds)->get();

        if(count($course) != 0){
            $lastIdx = $course[count($course)-1]->id;
        }else{
            $lastIdx = 0;
        }

        return view('dosen.addCourse', compact('dosen', 'course', 'lastIdx'));

    }

    public function save_mata_kuliah(Request $request){

        $dosen = Teacher::where('id', $request->id)->first();

        $course = Course::create([
            'nama_mk' => $request->nama,
            'SKS' => $request->sks,
        ]);

        $dosen->courses()->attach($course);

        return redirect()->route('detail.course', $dosen->id);
    }

    public function save_mata_kuliah_2(Request $request){

        $dosen = Teacher::where('id', $request->id)->first();

        $courses = $request->input('courses');

        $mkIds = [];

        foreach($courses as $course){
            $mkIds [] = $course;
        }

        $dosen->courses()->attach($mkIds);

        return redirect()->route('detail.course', $dosen->id);
    }

    public function remove_course($id){

        $course = Course::where('id', $id)->first();

        $course->teachers()->detach();

        return redirect()->back();

    }

}
