<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Teacher;
use App\Student;
use App\Education;
use App\Course;

class UserController extends Controller
{
    public function login(){

        return view('login.login');
    }

    public function post_login (Request $request){

        $attempts = [
            'email' => $request->email,
            'password' => $request->pass,
        ];

        $user = User::where('email', $request->email)->first();

        if($user->role == "ADMIN" && \Auth::attempt($attempts)){
            return redirect()->route('admin');

        }

        return redirect()->back();
    }

    public function index(){

        $teachers = Teacher::count();
        $students = Student::count();
        $educations = Education::count();
        $courses = Course::count();

        return view('dashboard', compact('teachers', 'students', 'educations', 'courses'));
    }

    public function logout(){

        \Auth::logout();

        return redirect()->route('login');
    }
}
