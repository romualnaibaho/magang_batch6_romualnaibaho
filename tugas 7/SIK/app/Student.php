<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'students';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_mhs', 'nim', 'jenis_kelamin', 'ttl',
    ];

    public function courses(){
        return $this->belongsToMany('App\Course', 'classrooms', 'student_id', 'course_id');
    }
}
