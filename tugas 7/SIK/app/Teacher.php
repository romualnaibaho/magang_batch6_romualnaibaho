<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'teachers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_dsn', 'NIP', 'gelar',
    ];

    public function educations(){
        return $this->belongsToMany('App\Education');
    }

    public function courses(){
        return $this->belongsToMany('App\Course');
    }
}
