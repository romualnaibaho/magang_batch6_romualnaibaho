@extends('index')

@section('content')
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Update Mata Kuliah {{  $course->nama_mk}}</h1>
            </div>

            <!-- Content Row -->
            <div class="add-btn d-flex justify-content-left">
                <a href="{{ route('show.mk') }}"><button name="add" class = "btn btn-secondary"><span class = "glyphicon glyphicon-plus"></span>< Kembali</button></a>
            </div>

        <div class="data mt-4 mb-4">
            <form method="POST" action="{{ route('save.update.mk') }}">
            @csrf
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $course->id }}" required>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Nama Mata Kuliah</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ $course->nama_mk }}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Jumlah SKS</label>
                        <input type="number" class="form-control" id="sks" name="sks" value="{{ $course->SKS }}" required>
                    </div>
                </div>
            
                <button type="submit" class="btn btn-primary">Update Data</button>
            </form>
        </div>
    </div>
    <!-- End of Main Content -->
    </div>
@endsection