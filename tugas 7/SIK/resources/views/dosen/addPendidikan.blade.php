@extends('index')

@section('content')
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Pilih Pendidikan</h1>
            </div>

            <!-- Content Row -->
            <div class="add-btn d-flex justify-content-left">
                <a href="{{ route('detail.pendidikan', $teacher->id) }}"><button name="add" class = "btn btn-secondary"><span class = "glyphicon glyphicon-plus"></span>< Kembali</button></a>
            </div>

        <div class="data mt-4 mb-4">
            <form method="POST" action="{{ route('save.selected.education') }}">
            @csrf
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $teacher->id }}">
                <table class="table table-bordered alert-warning table-hover">
                    <thead>
                        <tr class="text-center">
                            <th scope="col">No.</th>
                            <th scope="col">Strata</th>
                            <th scope="col">Jurusan</th>
                            <th scope="col">Sekolah</th>
                            <th scope="col">Tahun Mulai</th>
                            <th scope="col">Tahun Selesai</th>
                            <th scope="col">Select</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1
                        @endphp
                        @forelse($pendidikan as $pendidikan)
                        <tr>
                            <th class="" scope="row">{{ $i++ }}</th>
                            <td>{{ $pendidikan->strata}}</td>
                            <td class="">{{ $pendidikan->jurusan }}</td>
                            <td class="">{{ $pendidikan->sekolah }}</td>
                            <td class="text-center">{{ $pendidikan->tahun_mulai }}</td>
                            <td class="text-center">{{ $pendidikan->tahun_selesai }}</td>
                            <td><center>
                                    <input type="checkbox" name="educations[]" value="{{ $pendidikan->id }}"/>
                                </center>
                            </td>
                        </tr>

                        @if($pendidikan->id == $lastIdx)
                            <tr>
                                <td class="text-center" colspan="7">
                                    <button type="submit" class="btn btn-outline-success">Add Selected Education</button>
                                </td>
                            </tr>
                        @elseif($lastIdx == 0)
                            <tr>
                                <td class="text-center" colspan="7">
                                    <button type="submit" class="btn btn-outline-success">Add Selected Education</button>
                                </td>
                            </tr>
                        @endif
                        @empty
                            <td colspan="7" class="text-center">No Education left to added !</td>
                        
                        @endforelse
                    </tbody>
                </table>
                
            </form>
        </div>

        <div class="text-center mt-4 mb-4">
            <h3>Or Add New Educations Here !</h3>
        </div>

        <div class="data mt-4 mb-4">
            <form method="POST" action="{{ route('save.pendidikan.dosen') }}">
            @csrf
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $teacher->id }}">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Strata</label>
                        <input type="text" class="form-control" id="strata" name="strata" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Jurusan</label>
                        <input type="text" class="form-control" id="jurusan" name="jurusan" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Sekolah</label>
                        <input type="text" class="form-control" id="sekolah" name="sekolah" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Tahun Mulai</label>
                        <input type="text" class="form-control" id="tahun_mulai" name="tahun_mulai" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Tahun Selesai</label>
                        <input type="text" class="form-control" id="tahun_selesai" name="tahun_selesai" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
        </div>
    </div>
    <!-- End of Main Content -->
    </div>
@endsection