@extends('index')

@section('content')
    <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Mata Kuliah Diampu Oleh {{ $teacher->nama_dsn }}</h1>
            </div>

            <!-- Content Row -->
            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="add-btn d-flex justify-content-left">
                        <a href="{{ route('show.dosen') }}"><button name="add" class = "btn btn-secondary"><span class = "glyphicon glyphicon-plus"></span>< Kembali</button></a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="add-btn d-flex justify-content-end">
                        <a href="{{ route('add.dosen.course', $teacher->id) }}"><button name="add" class = "btn btn-primary"><span class = "glyphicon glyphicon-plus"></span>+ Ampuh Mata Kuliah</button></a>
                    </div>
                </div>
            </div>

        <div class="data mt-4">
            <table class="table table-bordered alert-warning table-hover">
                <thead>
                    <tr class="text-center">
                        <th scope="col">No.</th>
                        <th scope="col">Nama Mata Kuliah</th>
                        <th scope="col">Jumlah SKS</th>
                        <th scope="col">Remove</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1
                    @endphp
                    @forelse($course as $course)
                    <tr>
                        <th class="text-center" scope="row">{{ $i++ }}</th>
                        <td>{{ $course->nama_mk}}</td>
                        <td class="text-center">{{ $course->SKS }}</td>
                        <td class="text-center"><a href="{{ route('remove.dosen.course', $course->id) }}"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                    @empty
                        <td class="text-center" colspan="4">No Course taken yet !</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- End of Main Content -->
    </div>

    @endsection