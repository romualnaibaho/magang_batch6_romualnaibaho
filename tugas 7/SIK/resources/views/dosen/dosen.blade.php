@extends('index')

@section('content')
    <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Dosen</h1>
            </div>

            <!-- Content Row -->
            <div class="add-btn d-flex justify-content-end">
                <a href="{{ route('add.dosen') }}"><button name="add" class = "btn btn-primary"><span class = "glyphicon glyphicon-plus"></span>+ Tambah Dosen Baru</button></a>
            </div>

        <div class="data mt-4">
            <table class="table table-bordered alert-warning table-hover">
                <thead>
                    <tr class="text-center">
                        <th scope="col">No.</th>
                        <th scope="col">Nama</th>
                        <th scope="col">NIP</th>
                        <th scope="col">Gelar</th>
                        <th scope="col">Pendidikan</th>
                        <th scope="col">Mengampu Mata Kuliah</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1
                    @endphp
                    @forelse($dosen as $dosen)
                    <tr>
                        <th class="text-center" scope="row">{{ $i++ }}</th>
                        <td>{{ $dosen->nama_dsn}}</td>
                        <td class="text-center">{{ $dosen->NIP }}</td>
                        <td class="text-center">{{ $dosen->gelar }}</td>
                        <td class="text-center"><a href="{{ route('detail.pendidikan', $dosen->id) }}">Detail Pendidikan</a></td>
                        <td class="text-center"><a href="{{ route('detail.course', $dosen->id) }}">Detail Mata Kuliah</a></td>
                        <td><center>
                            <a href="{{ route('update.dosen', $dosen->id) }}" class = "btn btn-warning"><span class = "glyphicon glyphicon-edit"></span> Update</a> | 
                            <a href="{{ route('delete.dosen', $dosen->id) }}" class = "btn btn-danger"><span class = "glyphicon glyphicon-trash"></span> Delete</a>
                            </center>
                        </td>
                    @empty
                        <td class="text-center" colspan="6">No Teacher added yet !</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- End of Main Content -->
    </div>

    @endsection