@extends('index')

@section('content')
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Update Dosen {{ $dosen->nama_dsn }}</h1>
            </div>

            <!-- Content Row -->
            <div class="add-btn d-flex justify-content-left">
                <a href="{{ route('show.dosen') }}"><button name="add" class = "btn btn-secondary"><span class = "glyphicon glyphicon-plus"></span>< Kembali</button></a>
            </div>

        <div class="data mt-4 mb-4">
            <form method="POST" action="{{ route('save.update.dosen') }}">
            @csrf
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $dosen->id }}" required>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Nama Lengkap</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ $dosen->nama_dsn }}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label >NIP</label>
                        <input type="text" class="form-control" id="NIP" name="NIP" value="{{ $dosen->NIP }}" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Gelar</label>
                        <input type="text" class="form-control" id="gelar" name="gelar" value="{{ $dosen->gelar }}" required>
                    </div>
                </div>
            
                <button type="submit" class="btn btn-primary">Update Data</button>
            </form>
        </div>
    </div>
    <!-- End of Main Content -->
    </div>
@endsection