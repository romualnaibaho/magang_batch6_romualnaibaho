@extends('index')

@section('content')
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Mahasiswa</h1>
            </div>

            <!-- Content Row -->
            <div class="add-btn d-flex justify-content-left">
                <a href="{{ route('show.mahasiswa') }}"><button name="add" class = "btn btn-secondary"><span class = "glyphicon glyphicon-plus"></span>< Kembali</button></a>
            </div>

        <div class="data mt-4 mb-4">
            <form method="POST" action="{{ route('save.mahasiswa') }}">
            @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Nama Lengkap</label>
                        <input type="text" class="form-control" id="nama" name="nama" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label >NIM</label>
                        <input type="text" class="form-control" id="nim" name="nim" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Jenis Kelamin</label>
                        <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" required>
                            <option value="Laki-Laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Tempat, Tanggal Lahir</label>
                        <input type="text" class="form-control" id="ttl" name="ttl" required>
                    </div>
                </div>
            
                <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
        </div>
    </div>
    <!-- End of Main Content -->
    </div>
@endsection