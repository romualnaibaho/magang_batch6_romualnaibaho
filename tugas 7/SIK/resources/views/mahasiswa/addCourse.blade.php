@extends('index')

@section('content')
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Pilih Mata Kuliah</h1>
            </div>

            <!-- Content Row -->
            <div class="add-btn d-flex justify-content-left">
                <a href="{{ route('detail.mk', $mahasiswa->id) }}"><button name="add" class = "btn btn-secondary"><span class = "glyphicon glyphicon-plus"></span>< Kembali</button></a>
            </div>

        <div class="data mt-4 mb-4">
            <form method="POST" action="{{ route('save.selected.courses') }}">
            @csrf
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $mahasiswa->id }}">
                <table class="table table-bordered alert-warning table-hover">
                    <thead>
                        <tr class="text-center">
                            <th scope="col">No.</th>
                            <th scope="col">Nama Mata Kuliah</th>
                            <th scope="col">Jumlah SKS</th>
                            <th scope="col">Select</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1
                        @endphp
                        @forelse($course as $course)
                        <tr>
                            <th class="text-center" scope="row">{{ $i++ }}</th>
                            <td>{{ $course->nama_mk}}</td>
                            <td class="text-center">{{ $course->SKS }}</td>
                            <td><center>
                                    <input type="checkbox" name="courses[]" value="{{ $course->id }}"/>
                                </center>
                            </td>
                        </tr>

                        @if($course->id == $lastIdx)
                            <tr>
                                <td class="text-center" colspan="7">
                                    <button type="submit" class="btn btn-outline-success">Add Selected Course</button>
                                </td>
                            </tr>
                        @elseif($lastIdx == 0)
                            <tr>
                                <td class="text-center" colspan="7">
                                    <button type="submit" class="btn btn-outline-success">Add Selected Course</button>
                                </td>
                            </tr>
                        @endif
                        @empty
                            <td colspan="7" class="text-center">No Course left to added !</td>
                        
                        @endforelse
                    </tbody>
                </table>
                
            </form>
        </div>

        <div class="text-center mt-4 mb-4">
            <h3>Or Add New Courses Here !</h3>
        </div>

        <div class="data mt-4 mb-4">
            <form method="POST" action="{{ route('save.mahasiswa.courses') }}">
            @csrf
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $mahasiswa->id }}">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Nama Mata Kuliah</label>
                        <input type="text" class="form-control" id="nama" name="nama" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Jumlah SKS</label>
                        <input type="number" class="form-control" id="sks" name="sks" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
        </div>
    </div>
    <!-- End of Main Content -->
    </div>

    <script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
      alert(msg);
    }
  </script>
@endsection