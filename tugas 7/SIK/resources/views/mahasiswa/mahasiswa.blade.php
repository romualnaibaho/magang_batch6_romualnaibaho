@extends('index')

@section('content')
    <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Mahasiswa</h1>
            </div>

            <!-- Content Row -->
            <div class="add-btn d-flex justify-content-end">
                <a href="{{ route('add.mahasiswa') }}"><button name="add" class = "btn btn-primary"><span class = "glyphicon glyphicon-plus"></span>+ Tambah Mahasiswa Baru</button></a>
            </div>

        <div class="data mt-4">
            <table class="table table-bordered alert-warning table-hover">
                <thead>
                    <tr class="text-center">
                        <th scope="col">No.</th>
                        <th scope="col">Nama</th>
                        <th scope="col">NIM</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Tempat, Tanggal Lahir</th>
                        <th scope="col">Mata Kuliah yang Diambil</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1
                    @endphp
                    @forelse($mahasiswa as $mahasiswa)
                    <tr>
                        <th class="text-center" scope="row">{{ $i++ }}</th>
                        <td>{{ $mahasiswa->nama_mhs}}</td>
                        <td class="text-center">{{ $mahasiswa->nim }}</td>
                        <td class="text-center">{{ $mahasiswa->jenis_kelamin }}</td>
                        <td class="text-center">{{ $mahasiswa->ttl }}</td>
                        <td class="text-center"><a href="{{ route('detail.mk', $mahasiswa->id) }}">View Courses</a></td>
                        <td><center>
                            <a href="{{ route('update.mahasiswa', $mahasiswa->id) }}" class = "btn btn-warning"><span class = "glyphicon glyphicon-edit"></span> Update</a> | 
                            <a href="{{ route('delete.mahasiswa', $mahasiswa->id) }}" class = "btn btn-danger"><span class = "glyphicon glyphicon-trash"></span> Delete</a>
                            </center>
                        </td>
                    @empty
                        <td class="text-center" colspan="6">No Student added yet !</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- End of Main Content -->
    </div>

    @endsection