@extends('index')

@section('content')
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        @include('topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Update Riwayat Pendidikan</h1>
            </div>

            <!-- Content Row -->
            <div class="add-btn d-flex justify-content-left">
                <a href="{{ route('show.pendidikan') }}"><button name="add" class = "btn btn-secondary"><span class = "glyphicon glyphicon-plus"></span>< Kembali</button></a>
            </div>

        <div class="data mt-4 mb-4">
            <form method="POST" action="{{ route('save.update.pendidikan') }}">
            @csrf
                <input type="hidden" class="form-control" id="id" name="id" value="{{ $pendidikan->id }}" required>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Strata</label>
                        <input type="text" class="form-control" id="strata" name="strata" value="{{ $pendidikan->strata }}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Jurusan</label>
                        <input type="text" class="form-control" id="jurusan" name="jurusan" value="{{ $pendidikan->jurusan }}" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Sekolah</label>
                        <input type="text" class="form-control" id="sekolah" name="sekolah" value="{{ $pendidikan->sekolah }}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Tahun Mulai</label>
                        <input type="text" class="form-control" id="tahun_mulai" name="tahun_mulai" value="{{ $pendidikan->tahun_mulai }}" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Tahun Selesai</label>
                        <input type="text" class="form-control" id="tahun_selesai" name="tahun_selesai" value="{{ $pendidikan->tahun_selesai }}" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Update Data</button>
            </form>
        </div>
    </div>
    <!-- End of Main Content -->
    </div>
@endsection