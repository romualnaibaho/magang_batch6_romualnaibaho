<?php

use Illuminate\Support\Facades\Route;
use App\Teacher;
use App\Student;
use App\Education;
use App\Course;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'UserController@login')->name('login')->middleware('guest');

Route::post('/login', 'UserController@post_login')->name('post.login')->middleware('guest');

Route::get('/logout', 'UserController@logout')->name('logout')->middleware('auth');

Route::get('/admin/dashboard', 'UserController@index')->name('index')->middleware('auth');

Route::get('/dosen', 'TeacherController@show_dosen')->name('show.dosen');

Route::get('/add/dosen', 'TeacherController@add_dosen')->name('add.dosen');

Route::post('/save/dosen', 'TeacherController@save_dosen')->name('save.dosen');

Route::get('/update/dosen/{id}', 'TeacherController@update_dosen')->name('update.dosen');

Route::post('/update/dosen/', 'TeacherController@save_update_dosen')->name('save.update.dosen');

Route::get('/delete/dosen/{id}', 'TeacherController@delete_dosen')->name('delete.dosen');

Route::get('/detail-pendidikan/{id}', 'TeacherController@pendidikan')->name('detail.pendidikan');

Route::get('/add-pendidikan/{id}', 'TeacherController@add_pendidikan')->name('add.dosen.pendidikan');

Route::post('/save-pendidikan-dosen', 'TeacherController@save_pendidikan')->name('save.pendidikan.dosen');

Route::post('/save-selected-education', 'TeacherController@save_pendidikan_2')->name('save.selected.education');

Route::get('/remove/pendidian/{id}', 'TeacherController@remove_pendidikan')->name('remove.pendidikan');

Route::get('/detail-course/{id}', 'TeacherController@course')->name('detail.course');

Route::get('/adddosen--mata-kuliah/{id}', 'TeacherController@add_mata_kuliah')->name('add.dosen.course');

Route::post('/save-dosen-courses', 'TeacherController@save_mata_kuliah')->name('save.dosen.courses');

Route::post('/save-dosen-selected-courses', 'TeacherController@save_mata_kuliah_2')->name('save.dosen.selected.courses');

Route::get('/remove/dosen/course/{id}', 'TeacherController@remove_course')->name('remove.dosen.course');

Route::get('/mahasiswa', 'StudentController@show_mahasiswa')->name('show.mahasiswa');

Route::get('/add/mahasiswa', 'StudentController@add_mahasiswa')->name('add.mahasiswa');

Route::post('/save/mahasiswa', 'StudentController@save_mahasiswa')->name('save.mahasiswa');

Route::get('/update/mahasiswa/{id}', 'StudentController@update_mahasiswa')->name('update.mahasiswa');

Route::post('/save/update/mahasiswa', 'StudentController@save_update_mahasiswa')->name('save.update.mahasiswa');

Route::get('/detele/mahasiswa/{id}', 'StudentController@delete_mahasiswa')->name('delete.mahasiswa');

Route::get('/detail-mata-kuliah/{id}', 'StudentController@mata_kuliah')->name('detail.mk');

Route::get('/add-mata-kuliah/{id}', 'StudentController@add_mata_kuliah')->name('add.mahasiswa.course');

Route::post('/save-mahasiswa-courses', 'StudentController@save_mata_kuliah')->name('save.mahasiswa.courses');

Route::post('/save-selected-courses', 'StudentController@save_mata_kuliah_2')->name('save.selected.courses');

Route::get('/remove/course/{id}', 'StudentController@remove_course')->name('remove.course');

Route::get('/pendidikan', 'EducationController@show_pendidikan')->name('show.pendidikan');

Route::get('/add/pendidikan', 'EducationController@add_pendidikan')->name('add.pendidikan');

Route::post('/save/pendidikan', 'EducationController@save_pendidikan')->name('save.pendidikan');

Route::get('/update/pendidikan/{id}', 'EducationController@update_pendidikan')->name('update.pendidikan');

Route::post('/save/update/pendidikan', 'EducationController@save_update_pendidikan')->name('save.update.pendidikan');

Route::get('/detele/pendidikan/{id}', 'EducationController@delete_pendidikan')->name('delete.pendidikan');

Route::get('/mata_kuliah', 'CourseController@show_mata_kuliah')->name('show.mk');

Route::get('/add/mata_kuliah', 'CourseController@add_mata_kuliah')->name('add.mk');

Route::post('/save/mata_kuliah', 'CourseController@save_mata_kuliah')->name('save.mk');

Route::get('/update/mata_kuliah/{id}', 'CourseController@update_mata_kuliah')->name('update.mk');

Route::post('/save/update/mata_kuliah', 'CourseController@save_update_mata_kuliah')->name('save.update.mk');

Route::get('/detele/mata_kuliah/{id}', 'CourseController@delete_mata_kuliah')->name('delete.mk');