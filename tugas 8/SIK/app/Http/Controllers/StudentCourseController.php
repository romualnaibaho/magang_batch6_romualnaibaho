<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Student;
use App\Models\Course;

class StudentCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = Student::where('id', $request->student)->first();

        $myCourse = $student->courses;

        $sum = 0;

        foreach($myCourse as $mk){
            $sum += $mk->SKS;
        }

        // $courses = $request->input('courses');
        $myCourses = Course::where('id', $request->course)->first();
        $courses [] = $myCourses->id;

        $mkIds = [];

        foreach($courses as $course){
            $mkIds [] = $course;
            $sks = Course::select('SKS')->where('id', $course)->first();
            $sum += $sks->SKS;
        }

        if($sum <= 24){
            $student->courses()->attach($mkIds);
        }else{
            return redirect()->back()->withSuccess('SKS Berlebih ! Kurangi pemilihan SKS menjadi kurang dari atau sama dengan 24');
        }

        // ---

        // $course = Course::where('id', $request->course)->first();

        // $student->courses()->attach($course);

        return redirect()->route('student_course.show', $request->student);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::where('id', $id)->first();

        $course_id = DB::table('classrooms')->where('student_id', $id)
        ->get()
        ->pluck('course_id');

        $course = Course::wherein('id', $course_id)->get();

        return view('student.course_index', compact('student', 'course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::select('id')->where('id', $id)->first();
        $student_id = $student->id;

        $courses = $student->courses;

        $courseIds = [];

        foreach($courses as $course){
            $courseIds [] = $course->id;
        }
        
        $course = Course::whereNotIn('id', $courseIds)->pluck('nama_mk', 'id');      
        
        return view('student.course_create', compact('course', 'student_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
