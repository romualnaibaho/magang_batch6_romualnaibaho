<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Teacher;
use App\Models\Course;

class TeacherCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacher = Teacher::where('id', $request->teacher)->first();

        $course = Course::where('id', $request->course)->first();

        $teacher->courses()->attach($course);

        return redirect()->route('teacher_course.show', $request->teacher);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::where('id', $id)->first();

        $course_id = DB::table('course_teacher')->where('teacher_id', $id)
        ->get()
        ->pluck('course_id');

        $course = Course::wherein('id', $course_id)->get();

        return view('teacher.course_index', compact('teacher', 'course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::select('id')->where('id', $id)->first();
        $teacher_id = $teacher->id;

        $courses = $teacher->courses;

        $courseIds = [];

        foreach($courses as $course){
            $courseIds [] = $course->id;
        }
        
        $course = Course::whereNotIn('id', $courseIds)->pluck('nama_mk', 'id');    
        
        return view('teacher.course_create', compact('course', 'teacher_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
