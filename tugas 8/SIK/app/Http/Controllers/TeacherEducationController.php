<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Teacher;
use App\Models\Education;

class TeacherEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacher = Teacher::where('id', $request->teacher)->first();

        $education = Education::where('id', $request->education)->first();

        $teacher->educations()->attach($education);

        return redirect()->route('teacher_education.show', $request->teacher);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::where('id', $id)->first();

        $education_id = DB::table('education_teacher')->where('teacher_id', $id)
        ->get()
        ->pluck('education_id');

        $education = Education::wherein('id', $education_id)->get();

        return view('teacher.education_index', compact('teacher', 'education'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::select('id')->where('id', $id)->first();
        $teacher_id = $teacher->id;

        $educations = $teacher->educations;

        $eduIds = [];

        foreach($educations as $education){
            $eduIds [] = $education->id;
        }  
        
        $education = Education::whereNotIn('id', $eduIds)->pluck('jurusan', 'id');      
        
        return view('teacher.education_create', compact('education', 'teacher_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Education $education)
    {
        dd($education);

        $education->teachers()->detach();

        return redirect()->back();
    }
}
