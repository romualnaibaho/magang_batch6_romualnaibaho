<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'courses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'course_id', 'nama_mk', 'SKS',
    ];

    public function teachers(){
        return $this->belongsToMany('App\Models\Teacher');
    }

    public function students(){
        return $this->belongsToMany('App\Models\Student', 'classrooms', 'course_id', 'student_id');
    }
}
