<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'education';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'strata', 'jurusan', 'sekolah', 'tahun_mulai', 'tahun_selesai',
    ];

    public function teachers(){
        return $this->belongsToMany('App\Models\Teacher');
    }
}
