<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'students';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_mhs', 'nim', 'jenis_kelamin', 'ttl',
    ];

    public function courses(){
        return $this->belongsToMany('App\Models\Course', 'classrooms', 'student_id', 'course_id');
    }
}
