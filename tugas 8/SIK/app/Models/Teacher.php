<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'teachers';

    // protected $searchableColumns = ['nama_dsn', 'NIP', 'gelar'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_dsn', 'NIP', 'gelar',
    ];

    public function educations(){
        return $this->belongsToMany('App\Models\Education');
    }

    public function courses(){
        return $this->belongsToMany('App\Models\Course');
    }
}
