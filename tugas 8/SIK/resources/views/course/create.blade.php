<x-volt-app title="Mata Kuliah">

    {!! form()->post(route('course.store')) !!}
    {!! form()->text('nama_mk')->label('Nama Mata Kuliah') !!}
    {!! form()->text('SKS')->label('Jumlah SKS') !!}
    {!! form()->link('Batal', route('course.index')) !!}
    {!! form()->submit('Simpan') !!}
    {!! form()->close() !!}

</x-volt-app>
