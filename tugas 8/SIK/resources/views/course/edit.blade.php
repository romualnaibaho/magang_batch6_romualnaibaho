<x-volt-app title="Course">

    {!! form()->bind($course)->put(route('course.update', $course->id)) !!}
    {!! form()->text('nama_mk')->label('Nama Mata Kuliah') !!}
    {!! form()->text('SKS')->label('Jumlah SKS') !!}
    {!! form()->link('Batal', route('course.index')) !!}
    {!! form()->submit('Perbarui') !!}
    {!! form()->close() !!}

</x-volt-app>
