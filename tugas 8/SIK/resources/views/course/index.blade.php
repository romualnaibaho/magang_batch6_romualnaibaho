<x-volt-app title="Course">

@include('css.myCss')

    <a href="{{ route('course.create') }}">
        <button type="button" class="btn btn-light">+ Tambah Mata Kuliah</button>
    </a>
    

    {!! Suitable::source($course)->columns([
        \Laravolt\Suitable\Columns\Numbering::make('No'),
        \Laravolt\Suitable\Columns\Text::make('nama_mk', 'Nama Mata Kuliah'),
        \Laravolt\Suitable\Columns\Text::make('SKS', 'Jumlah SKS'),
        \Laravolt\Suitable\Columns\RestfulButton::make('course', 'Action')->except("show")
        ])->render()
    !!}

    <a href="{{ route('home') }}">
        <button type="button" class="btn btn-light">< Kembali</button>
    </a>

</x-volt-app>
