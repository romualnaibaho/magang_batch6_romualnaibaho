<x-volt-app title="Pendidikan">

    {!! form()->post(route('education.store')) !!}
    {!! form()->text('strata')->label('Strata') !!}
    {!! form()->text('jurusan')->label('Jurusan') !!}
    {!! form()->text('sekolah')->label('Sekolah') !!}
    {!! form()->text('tahun_mulai')->label('Tahun Mulai') !!}
    {!! form()->text('tahun_selesai')->label('Tahun Selesai') !!}
    {!! form()->link('Batal', route('education.index')) !!}
    {!! form()->submit('Simpan') !!}
    {!! form()->close() !!}

</x-volt-app>
