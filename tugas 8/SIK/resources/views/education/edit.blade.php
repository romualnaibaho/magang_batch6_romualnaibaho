<x-volt-app title="Pendidikan">

    {!! form()->bind($education)->put(route('education.update', $education->id)) !!}
    {!! form()->text('strata')->label('Strata') !!}
    {!! form()->text('jurusan')->label('Jurusan') !!}
    {!! form()->text('sekolah')->label('Sekolah') !!}
    {!! form()->text('tahun_masuk')->label('Tahun Masuk') !!}
    {!! form()->text('tahun_selesai')->label('Tahun Selesai') !!}
    {!! form()->link('Batal', route('education.index')) !!}
    {!! form()->submit('Perbarui') !!}
    {!! form()->close() !!}

</x-volt-app>
