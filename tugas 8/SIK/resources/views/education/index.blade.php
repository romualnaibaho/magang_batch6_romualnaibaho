<x-volt-app title="Pendidikan">

@include('css.myCss')

    <a href="{{ route('education.create') }}">
        <button type="button" class="btn btn-light">+ Tambah Riwayat Pendidikan</button>
    </a>
    

    {!! Suitable::source($education)->columns([
        \Laravolt\Suitable\Columns\Numbering::make('No'),
        \Laravolt\Suitable\Columns\Text::make('strata', 'Strata'),
        \Laravolt\Suitable\Columns\Text::make('jurusan', 'Jurusan'),
        \Laravolt\Suitable\Columns\Text::make('sekolah', 'Sekolah'),
        \Laravolt\Suitable\Columns\Text::make('tahun_mulai', 'Tahun Mulai'),
        \Laravolt\Suitable\Columns\Text::make('tahun_selesai', 'Tahun Selesai'),
        \Laravolt\Suitable\Columns\RestfulButton::make('education', 'Action')->except("show")
        ])->render()
    !!}

    <a href="{{ route('home') }}">
        <button type="button" class="btn btn-light">< Kembali</button>
    </a>

</x-volt-app>
