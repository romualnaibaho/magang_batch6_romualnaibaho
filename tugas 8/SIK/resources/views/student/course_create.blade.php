<x-volt-app title="Mahasiswa">

    {!! form()->post(route('student_course.store')) !!}
    {!! form()->text('student', $student_id)->attributes(['type' => 'hidden']) !!}
    {!! form()->dropdown('course', $course)->label('Mata Kuliah') !!}
    {!! form()->link('Batal', route('student_course.show', $student_id)) !!}
    {!! form()->submit('Simpan') !!}
    {!! form()->close() !!}

</x-volt-app>
