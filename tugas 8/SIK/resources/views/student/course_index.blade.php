<x-volt-app title="Mahasiswa">

@include('css.myCss')

    <a href="{{ route('student_course.edit', $student->id) }}">
        <button type="button" class="btn btn-light">+ Tambah Mata Kuliah</button>
    </a>

    <br><br>

    <h2>Mata Kuliah yang Diambil oleh {{$student->nama_mhs }}</h2>

    {!! Suitable::source($course)->columns([
        \Laravolt\Suitable\Columns\Numbering::make('No'),
        \Laravolt\Suitable\Columns\Text::make('nama_mk', 'Nama Mata Kuliah'),
        \Laravolt\Suitable\Columns\Text::make('SKS', 'Jumlah SKS'),
        \Laravolt\Suitable\Columns\RestfulButton::make('course', 'Action')->only("destroy")
        ])->render()
    !!}

    <a href="{{ route('student.index') }}">
        <button type="button" class="btn btn-light">< Kembali</button>
    </a>

</x-volt-app>
