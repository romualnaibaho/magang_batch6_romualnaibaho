<x-volt-app title="Mahasiswa">

    {!! form()->post(route('student.store')) !!}
    {!! form()->text('nama_mhs')->label('Nama Lengkap') !!}
    {!! form()->text('nim')->label('NIM') !!}
    {!! form()->text('jenis_kelamin')->label('Jenis Kelamin') !!}
    {!! form()->text('ttl')->label('Tempat, Tanggal Lahir') !!}
    {!! form()->link('Batal', route('student.index')) !!}
    {!! form()->submit('Simpan') !!}
    {!! form()->close() !!}

</x-volt-app>
