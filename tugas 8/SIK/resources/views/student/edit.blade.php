<x-volt-app title="Mahasiswa">

    {!! form()->bind($student)->put(route('student.update', $student->id)) !!}
    {!! form()->text('nama_mhs')->label('Nama Lengkap') !!}
    {!! form()->text('nim')->label('NIM') !!}
    {!! form()->text('jenis_kelamin')->label('Jenis Kelamin') !!}
    {!! form()->text('ttl')->label('Tempat, Tanggal Lahir') !!}
    {!! form()->link('Batal', route('student.index')) !!}
    {!! form()->submit('Perbarui') !!}
    {!! form()->close() !!}

</x-volt-app>
