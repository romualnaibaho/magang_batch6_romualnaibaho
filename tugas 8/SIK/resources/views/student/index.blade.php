<x-volt-app title="Mahasiswa">

@include('css.myCss')

    <a href="{{ route('student.create') }}">
        <button type="button" class="btn btn-light">+ Tambah Mahasiswa</button>
    </a>
    

    {!! Suitable::source($student)->columns([
        \Laravolt\Suitable\Columns\Numbering::make('No'),
        \Laravolt\Suitable\Columns\Text::make('nama_mhs', 'Nama Lengkap'),
        \Laravolt\Suitable\Columns\Text::make('nim', 'NIM'),
        \Laravolt\Suitable\Columns\Text::make('jenis_kelamin', 'Jenis Kelamin'),
        Laravolt\Suitable\Columns\Text::make('ttl', 'Tempat, Tanggal Lahir'),
        ['header' => 'Detail Mata Kuliah', 'raw' => function ($student) {
            return sprintf('<a href="student_course/%d">Lihat Mata Kuliah</a>', $student->id);
        }],
        \Laravolt\Suitable\Columns\RestfulButton::make('student', 'Action')->except("show")
        ])->render()
    !!}

    <a href="{{ route('home') }}">
        <button type="button" class="btn btn-light">< Kembali</button>
    </a>

</x-volt-app>
