<x-volt-app title="Dosen">

    {!! form()->post(route('teacher_course.store')) !!}
    {!! form()->text('teacher', $teacher_id)->attributes(['type' => 'hidden']) !!}
    {!! form()->dropdown('course', $course)->label('Mata Kuliah') !!}
    {!! form()->link('Batal', route('teacher_course.show', $teacher_id)) !!}
    {!! form()->submit('Simpan') !!}
    {!! form()->close() !!}

</x-volt-app>
