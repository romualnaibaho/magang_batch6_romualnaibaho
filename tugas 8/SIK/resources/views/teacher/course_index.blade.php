<x-volt-app title="Dosen">

@include('css.myCss')

    <a href="{{ route('teacher_course.edit', $teacher->id) }}">
        <button type="button" class="btn btn-light">+ Tambah Mata Kuliah Untuk Diampu</button>
    </a>

    <br><br>

    <h2>Mata Kuliah yang Diampu oleh {{$teacher->nama_dsn }}</h2>

    {!! Suitable::source($course)->columns([
        \Laravolt\Suitable\Columns\Numbering::make('No'),
        \Laravolt\Suitable\Columns\Text::make('nama_mk', 'Nama Mata Kuliah'),
        \Laravolt\Suitable\Columns\Text::make('SKS', 'Jumlah SKS'),
        \Laravolt\Suitable\Columns\RestfulButton::make('course', 'Action')->only("destroy")
        ])->render()
    !!}

    <a href="{{ route('teacher.index') }}">
        <button type="button" class="btn btn-light">< Kembali</button>
    </a>

</x-volt-app>
