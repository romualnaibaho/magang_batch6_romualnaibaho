<x-volt-app title="Dosen">

    {!! form()->post(route('teacher.store')) !!}
    {!! form()->text('nama_dsn')->label('Nama Lengkap') !!}
    {!! form()->text('NIP')->label('NIP') !!}
    {!! form()->text('gelar')->label('Gelar') !!}
    {!! form()->link('Batal', route('teacher.index')) !!}
    {!! form()->submit('Simpan') !!}
    {!! form()->close() !!}

</x-volt-app>
