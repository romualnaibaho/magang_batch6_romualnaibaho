<x-volt-app title="Dosen">

    {!! form()->bind($teacher)->put(route('teacher.update', $teacher->id)) !!}
    {!! form()->text('nama_dsn')->label('Nama Lengkap') !!}
    {!! form()->text('NIP')->label('NIP') !!}
    {!! form()->text('gelar')->label('Gelar') !!}
    {!! form()->link('Batal', route('teacher.index')) !!}
    {!! form()->submit('Perbarui') !!}
    {!! form()->close() !!}

</x-volt-app>
