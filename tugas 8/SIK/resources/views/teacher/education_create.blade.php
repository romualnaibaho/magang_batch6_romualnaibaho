<x-volt-app title="Dosen">

    {!! form()->open(route('teacher_education.store')) !!}
    {!! form()->text('teacher', $teacher_id)->attributes(['type' => 'hidden']) !!}
    {!! form()->dropdown('education', $education)->label('Jurusan') !!}
    {!! form()->link('Batal', route('teacher_education.show', $teacher_id)) !!}
    {!! form()->submit('Simpan') !!}
    {!! form()->close() !!}

</x-volt-app>
