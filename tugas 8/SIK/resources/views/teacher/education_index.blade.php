<x-volt-app title="Dosen">

@include('css.myCss')

    <a href="{{ route('teacher_education.edit', $teacher->id) }}">
        <button type="button" class="btn btn-light">+ Tambah Riwayat Pendidikan</button>
    </a>

    <h2>Riwayat Pendidikan {{$teacher->nama_dsn }}</h2>
    

    {!! Suitable::source($education)->columns([
        \Laravolt\Suitable\Columns\Numbering::make('No'),
        \Laravolt\Suitable\Columns\Text::make('strata', 'Strata'),
        \Laravolt\Suitable\Columns\Text::make('jurusan', 'Jurusan'),
        \Laravolt\Suitable\Columns\Text::make('sekolah', 'Sekolah'),
        \Laravolt\Suitable\Columns\Text::make('tahun_mulai', 'Tahun Mulai'),
        \Laravolt\Suitable\Columns\Text::make('tahun_selesai', 'Tahun Selesai'),
        \Laravolt\Suitable\Columns\RestfulButton::make('education', 'Action')->only("destroy")
        ])->render()
    !!}

    <a href="{{ route('teacher.index') }}">
        <button type="button" class="btn btn-light">< Kembali</button>
    </a>

</x-volt-app>
