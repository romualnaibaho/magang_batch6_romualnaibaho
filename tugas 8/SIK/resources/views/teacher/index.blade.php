<x-volt-app title="Dosen">

@include('css.myCss')

    <a href="{{ route('teacher.create') }}">
        <button type="button" class="btn btn-light">+ Tambah Dosen</button>
    </a>

    <br><br>

    {!! Suitable::source($teacher)->columns([
        \Laravolt\Suitable\Columns\Numbering::make('No'),
        \Laravolt\Suitable\Columns\Text::make('nama_dsn', 'Nama Lengkap'),
        \Laravolt\Suitable\Columns\Text::make('NIP', 'NIP'),
        \Laravolt\Suitable\Columns\Text::make('gelar', 'Gelar'),
        ['header' => 'Detail Pendidikan', 'raw' => function ($teacher) {
            return sprintf('<a href="teacher_education/%d">Lihat Detail Pendidikan</a>', $teacher->id);
        }],
        ['header' => 'Detail Mata Kuliah', 'raw' => function ($teacher) {
            return sprintf('<a href="teacher_course/%d">Lihat Mata Kuliah yang Diampu</a>', $teacher->id);
        }],
        \Laravolt\Suitable\Columns\RestfulButton::make('teacher', 'Action')->except("show")
        ])->render()
    !!}

    <a href="{{ route('home') }}">
        <button type="button" class="btn btn-light">< Kembali</button>
    </a>
</x-volt-app>
