<?php

use App\Http\Controllers\Home;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\EducationController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\TeacherEducationController;
use App\Http\Controllers\TeacherCourseController;
use App\Http\Controllers\StudentCourseController;

Route::redirect('/', 'auth/login');

Route::middleware(['auth', 'verified'])
    ->group(
        function () {
            Route::get('/home', Home::class)->name('home');
        }
    );

Route::resource('teacher', TeacherController::class);

Route::resource('teacher_education', TeacherEducationController::class);

Route::resource('teacher_course', TeacherCourseController::class);

Route::resource('student', StudentController::class);

Route::resource('student_course', StudentCourseController::class);

Route::resource('education', EducationController::class);

Route::resource('course', CourseController::class);

include __DIR__.'/auth.php';
include __DIR__.'/my.php';
